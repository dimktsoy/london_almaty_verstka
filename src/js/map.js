ymaps.ready(function () {
    // Создание экземпляра карты и его привязка к созданному контейнеру.
    var myMap = new ymaps.Map('map', {
            center: [43.2620, 76.9278],
            zoom: 12,
            controls: []
        });

        myMap.behaviors.disable('scrollZoom');

        myMap.controls.add("zoomControl", {
            position: {top: 15, left: 15}
        });

        // Создание макета балуна.
        MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
            '<div class="popover top">' +
                '<a class="close" href="#">&times;</a>' +
                '<div class="arrow"></div>' +
                '<div class="popover-inner">' +
                '$[[options.contentLayout observeSize minWidth=235 maxWidth=400 maxHeight=auto]]' +
                '</div>' +
                '</div>', {
                /**
                 * Строит экземпляр макета на основе шаблона и добавляет его в родительский HTML-элемент.
                 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#build
                 * @function
                 * @name build
                 */
                build: function () {
                    this.constructor.superclass.build.call(this);

                    this._$element = $('.popover', this.getParentElement());

                    this.applyElementOffset();

                    this._$element.find('.close')
                        .on('click', $.proxy(this.onCloseClick, this));
                },

                /**
                 * Удаляет содержимое макета из DOM.
                 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#clear
                 * @function
                 * @name clear
                 */
                clear: function () {
                    this._$element.find('.close')
                        .off('click');

                    this.constructor.superclass.clear.call(this);
                },

                /**
                 * Метод будет вызван системой шаблонов АПИ при изменении размеров вложенного макета.
                 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
                 * @function
                 * @name onSublayoutSizeChange
                 */
                onSublayoutSizeChange: function () {
                    MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);

                    if(!this._isElement(this._$element)) {
                        return;
                    }

                    this.applyElementOffset();

                    this.events.fire('shapechange');
                },

                /**
                 * Сдвигаем балун, чтобы "хвостик" указывал на точку привязки.
                 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
                 * @function
                 * @name applyElementOffset
                 */
                applyElementOffset: function () {
                    this._$element.css({
                        left: -(this._$element[0].offsetWidth / 2),
                        top: -(this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight)
                    });
                },

                /**
                 * Закрывает балун при клике на крестик, кидая событие "userclose" на макете.
                 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
                 * @function
                 * @name onCloseClick
                 */
                onCloseClick: function (e) {
                    e.preventDefault();

                    this.events.fire('userclose');
                },

                /**
                 * Используется для автопозиционирования (balloonAutoPan).
                 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/ILayout.xml#getClientBounds
                 * @function
                 * @name getClientBounds
                 * @returns {Number[][]} Координаты левого верхнего и правого нижнего углов шаблона относительно точки привязки.
                 */
                getShape: function () {
                    if(!this._isElement(this._$element)) {
                        return MyBalloonLayout.superclass.getShape.call(this);
                    }

                    var position = this._$element.position();

                    return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
                        [position.left, position.top], [
                            position.left + this._$element[0].offsetWidth,
                            position.top + this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight
                        ]
                    ]));
                },

                /**
                 * Проверяем наличие элемента (в ИЕ и Опере его еще может не быть).
                 * @function
                 * @private
                 * @name _isElement
                 * @param {jQuery} [element] Элемент.
                 * @returns {Boolean} Флаг наличия.
                 */
                _isElement: function (element) {
                    return element && element[0] && element.find('.arrow')[0];
                }
            }),

    // Создание вложенного макета содержимого балуна.
        MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass(
            '<h3 class="popover-title">$[properties.balloonHeader]</h3>' +
            '<div class="popover-content">$[properties.balloonContent]</div>'
        ),

    // Создание метки с пользовательским макетом балуна.
        myPlacemark = window.myPlacemark = new ymaps.Placemark([43.263094, 76.927202], {
            balloonHeader: 'Подгородецкий и партнеры',
            balloonContent: '<div class="balloon-col1">' +
                                '<p>' +
                                    '<strong>Адрес:</strong><br>' +
                                    'г. Алматы, пр-т. Суюнбая, 159а' +
                                '</p>' +
                                '<p>' +
                                    '<strong>Телефоны:</strong><br>' +
                                    '(+7 727) 300 55 33, 300 55 22, 300 55 77' +
                                '</p>' +
                                '<p>' +
                                    '<strong>График работы::</strong><br>' +
                                    'Пн-Пт: 10:00–22:00<br>Сб: 10:00–18:00<br><span class="text-gray">Вс: Выходной</span>' +
                                '</p>' +
                            '</div>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'http://london-almaty.neoweb.kz/wp-content/themes/london-almaty/img/point.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33],
            balloonShadow: false,
            balloonLayout: MyBalloonLayout,
            balloonContentLayout: MyBalloonContentLayout,
            balloonPanelMaxMapArea: 0
        });

    myMap.geoObjects.add(myPlacemark);
    //
    // // Создание метки с пользовательским макетом балуна.
    //     myPlacemark1 = window.myPlacemark = new ymaps.Placemark([43.251689, 76.901169], {
    //         balloonHeader: 'Этот центр еще строится',
    //         balloonContent: '<p>' +
    //                             '<strong>Адрес:</strong><br>' +
    //                             'г. Алматы, ул. Макатаева, 117, Блок А, офис 520' +
    //                         '</p>' +
    //                         '<p>' +
    //                             '<strong>Дата запуска:</strong><br>' +
    //                             '12 ноября 2016 г' +
    //                         '</p>' +
    //                         '<p>' +
    //                             '<strong>Мест:</strong><br>' +
    //                             '451 место для тестирования' +
    //                         '</p>'
    //     }, {
    //         iconLayout: 'default#image',
    //         iconImageHref: 'http://london-almaty.neoweb.kz/wp-content/themes/london-almaty/img/point-const.png',
    //         iconImageSize: [37, 49],
    //         iconImageOffset: [-18, -45],
    //         balloonShadow: false,
    //         balloonLayout: MyBalloonLayout,
    //         balloonContentLayout: MyBalloonContentLayout,
    //         balloonPanelMaxMapArea: 0
    //     });
    //
    // myMap.geoObjects.add(myPlacemark1);
});
